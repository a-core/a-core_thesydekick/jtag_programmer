"""
jtag_programmer
===============

A simple microprocessor memory programmer. Reads in an ELF file, parses its
program data to [address data] colums, and generates a programming sequence
of writes to appropriate JTAG Test Data Registers to transfer the program
data to the device. The ``address`` and ``data`` values are simply written
to corresponding TDRs. An additional write enable signal ``write_en`` is
also generated to coordinate the writes to the target memory.

Example
-------

See ``jtag_programmer/__init__.py`` self-test for an example.

"""

import os
import sys
import yaml
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import *
from rtl import *
from rtl.module import *

from elftools.elf.elffile import ELFFile
import numpy as np
from jtag import *
from jtag.driver import jtag_driver
import jtag_kernel


# helper to call functions in jtag_driver
# inherits thesdk for logging
class jtag_programmer(thesdk):
    """
    The ``jtag_programmer`` is a helper class for uploading program data to
    systems over the JTAG serial interface. This class uses the ``jtag_driver``
    entity to generate a simulation excitation for a control sequence that
    uploads the program data to the design over a simple address, data, write_enable
    interface.

    Parameters
    ----------
    driver: jtag_driver
        An instance of ``jtag_driver`` used to generate the programming waveform.

    config: dict
        YAML based JTAG configuration 

    namemap: dict
        A dict that maps "address", "data", and "write_en" to the corresponding
        TDR names in the JTAG config YAML.

    Attributes
    ----------

    driver: jtag_driver
        An instance of ``jtag_driver`` used to generate the programming waveform.

    """
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self, driver, config, namemap):
        self.print_log(type='I', msg='Inititalizing %s' %(__name__)) 
        self.proplist = [ 'Rs' ];    # Properties that can be propagated from parent
        self.Rs = 100e6;             # Sampling frequency

        # sanity checks
        assert isinstance(driver, jtag_driver), "argument 'driver' must be an instance of jtag_driver"
        self._validate_namemap(namemap)

        self.driver = driver
        self.config = config
        self.namemap = namemap
        self.elf_data = None

        #file for controller is created in controller
        self.model='py';             # Can be set externally, but is not propagated

        self.init()

    def _validate_namemap(self, namemap):
        # Assert keys are defined
        assert 'address' in namemap, "Missing required key: 'address'"
        assert 'data' in namemap, "Missing required key: 'data'"
        assert 'write_en' in namemap, "Missing required key: 'write_en'"

    def init(self):
        """
        Re-initialize the structure if the attribute values are changed after creation.

        """
        pass
   
    def main(self):
        """
        Writes the program data of the read elf-file to JTAG UpdateChains

        """
        if self.elf_data is None:
            self.print_log(type='W', msg="main(): No data to generate programming waveform from!")
            self.print_log(type='W', msg="main(): Use read_elf() first to load in program data from a file.")
            self.print_log(type='F', msg="main(): Exiting...")

        addr_config = self._tdr_config_by_name(self.namemap['address'])
        data_config = self._tdr_config_by_name(self.namemap['data'])
        wen_config = self._tdr_config_by_name(self.namemap['write_en'])

        # drive to idle and start blasting programming data
        self.driver.tms_reset()
        self.driver.reset_to_idle()
        self.print_log(type='I', msg="Generating programming waveform with `jtag_kernel`")
        jtag_kernel.set_programmer_config(
            self.config['ir_width'],    # len_ir
            addr_config['ir'],          # ir_addr
            data_config['ir'],          # ir_data
            wen_config['ir'],           # ir_wen
            addr_config['width'],       # len_addr
            data_config['width'],       # len_data
            wen_config['width'],        # len_wen
            self.driver.trstn_polarity  # trstn_polarity
        )
        jtag_kernel.gen_prog_waveform(
            self.elf_data.astype(np.uint32)
        )
        self.remote_bitbang = jtag_kernel.get_remote_bitbang()
        self.print_log(type='I', msg="len(self.remote_bitbang) = %d" % (len(self.remote_bitbang)))

    def read_instruction_bytes(self, filepath):
        """
        Load program data to memory from the ELF file located at ``filepath``.
        The loaded data is stored as an numpy array with one byte per item

        Parameters
        ----------
        filepath : str
            Path to program ELF file to be loaded.

        """
        self.print_log(type='I', msg="Reading program data from %s" % os.path.abspath(filepath))
        elffile = None
        try:
            fd = open(filepath, 'rb')
            elffile = ELFFile(fd)
        except Exception as e:
            self.print_log(type='F', msg="Could not open %s : %s" %(filepath, e))

        # Get program instructions as a byte string
        sections = ['.text', '.data', '.sdata', '.rodata']
        rom_bytes = b''
        for sect_name in sections:
            section = elffile.get_section_by_name(sect_name)
            if section is not None:
                rom_bytes += section.data()
        return np.array([b for b in rom_bytes]).astype(int)

    def read_elf(self, filepath):
        """
        Load program data to memory from the ELF file located at ``filepath``.
        The loaded data is stored as an numpy array with adderss and data columns.

        As a side effect this method sets the ``self.jtag_steps`` variable to contain
        the time required by the JTAG programming sequence.
        

        Parameters
        ----------
        filepath : str
            Path to program ELF file to be loaded.

        """
        indata = self.read_instruction_bytes(filepath)
        self.program_size = len(indata)

        # Modelsim stimuli for programming
        #text_start = text_section.header['sh_addr']
        write_addr = np.arange(self.program_size).astype(int)

        # Assign vales to programming interface: address, data
        self.elf_data=np.r_['1', write_addr.reshape(-1,1), indata.reshape(-1,1) ].reshape(-1,2)

    # Look up tdr config dict by name
    def _tdr_config_by_name(self, name):
        for tdr in self.config['tdrs']:
            if tdr['name'] == name:
                return tdr
        return None

    def run(self):
        """
        Main entity runner.

        """
        if self.model=='py':
            self.main()
        else:
            self.print_log(type='F', msg='%s model currently not supported' % self.model) 


# self-test
if __name__=="__main__":
    pass
