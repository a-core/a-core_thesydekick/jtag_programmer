# simulation controls
add wave -divider "Simulation Controls"
add wave -label initdone sim/:tb_JTAG:initdone

# jtag tap
add wave -divider "JTAG TAP"
add wave -label TCK sim/:tb_JTAG:tapio_TCK
add wave -label TMS sim/:tb_JTAG:tapio_TMS
add wave -label TDI sim/:tb_JTAG:tapio_TDI
add wave -label TRSTn sim/:tb_JTAG:tapio_TRSTn
add wave -label TDO_data sim/:tb_JTAG:tapio_TDO_data
add wave -label TDO_driven sim/:tb_JTAG:tapio_TDO_driven

add wave -divider "JTAG TAP Controller"
radix define TapControllerState {
    4'b0000 "Exit2-DR"
    4'b0001 "Exit1-DR"
    4'b0010 "Shift-DR"
    4'b0011 "Pause-DR"
    4'b0100 "Select-IR-Scan"
    4'b0101 "Update-DR"
    4'b0110 "Capture-DR"
    4'b0111 "Select-DR-Scan"
    4'b1000 "Exit2-IR"
    4'b1001 "Exit1-IR"
    4'b1010 "Shift-IR"
    4'b1011 "Pause-IR"
    4'b1100 "Run-Test/Idle"
    4'b1101 "Update-IR"
    4'b1110 "Capture-IR"
    4'b1111 "Test-Logic Reset"
}
add wave -label state sim/:tb_JTAG:JTAG:TAP_controllerInternal:stateMachine:currState
radix signal sim/:tb_JTAG:JTAG:TAP_controllerInternal:stateMachine:currState TapControllerState

add wave -divider "TDR Outputs"
add wave -label prog_iface_addr sim/:tb_JTAG:tdrio_out_1
add wave -label prog_iface_data sim/:tb_JTAG:tdrio_out_2
add wave -label prog_iface_wen sim/:tb_JTAG:tdrio_out_3 
radix signal sim/:tb_JTAG:tdrio_out_1 hexadecimal
radix signal sim/:tb_JTAG:tdrio_out_2 hexadecimal

# run simulation
onfinish stop
run -all

# go back to waveform viewer and zoom to fit
view wave
wave zoom full
